package com.sns.docsapp.DB;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePrefHelper {
    private static SharePrefHelper mSharePrefHelper;
    private Context mContext;

    private final String SHAR_PREF_NAME = "DOCS_APP";
    private final String IS_OFFLINE_CHAT_AVAILABLE = "is_offline_chat";

    private SharePrefHelper(Context context) {
        this.mContext = context;
    }

    public static SharePrefHelper getInstance(Context context) {
        if (mSharePrefHelper == null) {
            mSharePrefHelper = new SharePrefHelper(context);
        }
        return mSharePrefHelper;
    }

    public void setOfflineMsg(boolean isOfflineChatAvailable) {
        SharedPreferences preferences = mContext.getSharedPreferences(SHAR_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(IS_OFFLINE_CHAT_AVAILABLE, isOfflineChatAvailable);
        editor.apply();
    }

    public boolean isOfflineMsg() {
        SharedPreferences preferences = mContext.getSharedPreferences(SHAR_PREF_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(IS_OFFLINE_CHAT_AVAILABLE, false);
    }
}
