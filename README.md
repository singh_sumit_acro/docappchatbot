# DocsAppRepo
Task Done:
## Start Date and time: 29 Sep 2019 13:10 PM
## Version 1.0
1. UI as what's app.
2. Network API hit and getting response.
3. Network handing using retrofit and gson.
4. Showing all chat on screen with its creating date and time.
5. handing UX like every new message auto scrools to latest message.
6. Date change handling.
7. Chat are displayinf basis of date(Latest message at bottom).

## Version 2.0
1. Persisting data both send and received in DB
2. Used SQLite for DB.
3. Added Hamburger menu to show a list.
4. selectable list, loading all chat history of respective list item.

## Version 3.0
1. Handle offline scenario.
2. When device comes online fetch all not synced chat and sync.
3. after syncing chat update chat status in DB.
4. Store response of chat in DB and show in chat screen.
5. Makeing sure all Tables chat will be synced.